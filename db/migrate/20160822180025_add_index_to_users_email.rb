class AddIndexToUsersEmail < ActiveRecord::Migration
  def change
    add_index :users, :email, unique: true #prevent the uniqueness violation in the db level (if user clicked create user twice sync)
  end
end
